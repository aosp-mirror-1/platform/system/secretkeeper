// Copyright 2023, The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package {
    default_applicable_licenses: ["system_secretkeeper_license"],
}

rust_defaults {
    name: "libsecretkeeper_core_defaults",
    lints: "android",
    vendor_available: true,
    defaults: [
        "authgraph_use_latest_hal_aidl_rust",
        "secretkeeper_use_latest_hal_aidl_rust",
    ],
    rustlibs: [
        // TODO(b/315464358): Use no_std version of authgraph_core/authgraph_wire/coset
        "libauthgraph_core",
        "libauthgraph_wire",
        "libciborium",
        "libcoset",
        "libdice_policy",
        "liblog_rust",
        "libsecretkeeper_comm_nostd",
    ],
}

rust_library {
    name: "libsecretkeeper_core_nostd",
    crate_name: "secretkeeper_core",
    srcs: ["src/lib.rs"],
    defaults: [
        "libsecretkeeper_core_defaults",
    ],
    no_stdlibs: true,
}

rust_test {
    name: "libsecretkeeper_core_test",
    crate_name: "secretkeeper_core_test",
    srcs: ["src/lib.rs"],
    defaults: [
        "libsecretkeeper_core_defaults",
    ],
    rustlibs: [
        "libhex",
    ],
    test_suites: ["general-tests"],
}
